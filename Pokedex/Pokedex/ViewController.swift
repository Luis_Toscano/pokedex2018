//
//  ViewController.swift
//  Pokedex
//
//  Created by LuisT on 13/6/18.
//  Copyright © 2018 LuisT. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    var pokemon:[Pokemon] = [];
    
    @IBOutlet weak var firePokemonTableView: UITableView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        let network = Network()
        
        network.getAllFireInitialPokemon { (allFireInitialPokemon) in
            self.pokemon = allFireInitialPokemon;
            self.firePokemonTableView.reloadData()
        }
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return pokemon.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        cell.textLabel?.text = pokemon[indexPath.row].name
        return cell
    }
    
    

}

