//
//  Networking.swift
//  Pokedex
//
//  Created by LuisT on 13/6/18.
//  Copyright © 2018 LuisT. All rights reserved.
//

import Foundation
import Alamofire

class Network {
    
    func getAllFireInitialPokemon (completion:@escaping ([Pokemon]) -> ()) {
        var fireInitialPokemonArray:[Pokemon] = []
        let group = DispatchGroup ()
        for i in [4, 155, 255] {
            group.enter()
            Alamofire.request("https://pokeapi.co/api/v2/pokemon/\(i)").responseJSON { response in
                guard let data = response.data else {
                    print("Error")
                    return
                }
                
                guard let pokemon = try? JSONDecoder().decode(Pokemon.self, from: data) else {
                    print("Error decoding Pokemon")
                    return
                }
                
                fireInitialPokemonArray.append(pokemon)
                group.leave()
            }
        }
        
        group.notify(queue: .main) {
            completion(fireInitialPokemonArray)
        }
    }
    
    func getAllPokemon () {
        
        Alamofire.request("https://pokeapi.co/api/v2/pokemon/4").responseJSON { (response) in
            
            
            guard let data = response.data else {
                print("Error")
                return
            }
            
            guard let pokemon = try? JSONDecoder().decode(Pokemon.self, from: response.data! ) else {
                print("Error decoding Pokemon")
                return
            }
            
            print(pokemon)
        }
    }
    
    func getPokemonImage(url: String) {
        
    }
}
